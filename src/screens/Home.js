import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
} from 'react-native';
import {Button} from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';

import {cars, cities} from '../../data';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    state = {
        selectedMark: false,
        selectedMarkIndex: '',
        modelArr: [],
        modelValue: '',

        selectedRegion: false,
        selectRegionIndex: '',
        citiesArr: [],
        cityValue: ''
    };

    changeMark = (item, index) => {
        const modelArr = [];

        cars[index - 1].models.forEach((item) => {
            modelArr.push({
                label: item.value,
                value: item.value,
            });
        });

        this.setState(() => {
            return {
                selectedMark: true,
                selectedMarkIndex: index,
                modelValue: '',
                modelArr,
            };
        });
    };

    changeRegion = (item, index) => {
        const citiesArr = [];

        cities[index - 1].cities.forEach((item) => {
            citiesArr.push({
                label: item.name,
                value: item.name,
            });
        });

        this.setState(() => {
            return {
                selectedRegion: true,
                selectRegionIndex: index,
                cityValue: '',
                citiesArr,
            };
        });
    };

    render() {
        const regionPlaceholder = {
            label: 'Выберите область',
            value: null,
        };

        const cityPlaceholder = {
            label: 'Выберите город',
            value: null,
        };

        const regionArr = [];

        cities.forEach(item => {
            regionArr.push({
                label: item.name,
                value: item.name,
                id: item.id,
            });
        });

        const markArr = [];

        cars.forEach(item => {
            markArr.push({
                label: item.value,
                value: item.value,
            });
        });

        return (
            <View>
                <View style={styles.container}>
                    <Text style={styles.label}>Марка</Text>
                    <RNPickerSelect
                        onValueChange={(value, index) => this.changeMark(value, index)}
                        items={markArr}
                    />
                </View>

                <View style={styles.container}>
                    <Text style={styles.label}>Модель</Text>

                    <RNPickerSelect
                        disabled={!this.state.selectedMark ? 'disabled' : ''}
                        onValueChange={(value) => this.setState({modelValue: value})}
                        items={this.state.modelArr}
                    />
                </View>

                <View style={styles.container}>
                    <Text style={styles.label}>Год</Text>

                    <View style={styles.containerInput}>
                        <TextInput
                            style={styles.textInput}/>
                        <Text> - </Text>
                        <TextInput
                            style={styles.textInput}/>
                    </View>

                </View>

                <View style={styles.container}>
                    <Text style={styles.label}>Цена</Text>

                    <View style={styles.containerInput}>
                        <TextInput
                            style={styles.textInput}/>
                        <Text> - </Text>
                        <TextInput
                            style={styles.textInput}/>
                    </View>

                </View>

                <View style={styles.container}>
                    <Text style={styles.label}>Область</Text>

                    <RNPickerSelect
                        placeholder={regionPlaceholder}
                        onValueChange={(value, index) => this.changeRegion(value, index)}
                        items={regionArr}
                    />
                </View>

                <View style={styles.container}>
                    <Text style={styles.label}>Город</Text>

                    <RNPickerSelect
                        placeholder={cityPlaceholder}
                        value={this.state.cityValue}
                        disabled={!this.state.selectedRegion ? 'disabled' : ''}
                        onValueChange={(value) => this.setState({cityValue: value})}
                        items={this.state.citiesArr}
                    />
                </View>

                <View style={styles.btnContainer}>
                    <Button
                        title="Поиск"
                    />
                </View>

            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 10,
        paddingHorizontal: 15,
    },

    containerInput: {
        flexDirection: 'row',
        alignItems: 'center',
    },

    label: {
        fontSize: 16,
    },

    textInput: {
        width: 80,
        padding: 8,
        borderColor: '#f5f5f5',
        backgroundColor: '#f5f5f5',
    },

    btnContainer: {
        marginTop: 50,
        paddingHorizontal: 70,
    },
});

export default Home;
